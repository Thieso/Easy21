import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from Easy21 import Easy21

dealer_s = [[1, 4], [4, 7], [7, 10]]
player_s = [[1, 6], [4, 9], [7, 12], [10, 15], [13, 18], [16, 21]]
action_s = [0, 1]

def theta(state, action):
    t = np.zeros((3, 6, 2))
    for i in range(len(dealer_s)):
        if state[0] >= dealer_s[i][0] and state[0] <= dealer_s[i][1]:
            for j in range(len(player_s)):
                if state[1] >= player_s[j][0] and state[1] <= player_s[j][1]:
                    t[i, j, action] = 1
    return np.reshape(t, (36, ))

def q(state, action, w):
    return np.dot(theta(state, action), w)

def compute_q(w):
    Q = np.zeros((10, 21, 2))
    for i in range(1, 11):
        for j in range(1, 22):
            for k in range(0, 2):
                Q[i-1, j-1, k] = q([i, j], k, w)
    return Q



def main():
    random.seed(123456789)
    # create game object
    easy21 = Easy21()

    # load correct value function
    Qmax_correct = np.genfromtxt('optimal_action_value_function.csv', delimiter=',')
    # init Q error array
    Q_error = []

    # for model free control
    nr_states_dealer = 10
    nr_states_player = 21
    nr_actions = 2
    N0 = 200
    w = np.random.rand(36)
    w_init = np.copy(w)

    nr_episodes = 5000
    lambda_values = np.arange(0, 1.1, 0.2)
    learning_curve = np.zeros((lambda_values.shape[0], nr_episodes+1))

    Q = compute_q(w)
    Qmax = np.max(Q, axis=2)
    e = np.sum((Qmax - Qmax_correct) ** 2)
    learning_curve[:, 0] = e

    for lambda_i in range(lambda_values.shape[0]):
        # init weights
        w = np.copy(w_init)
        for nr_episode in range(nr_episodes):
            # print(nr_episode, " / ", nr_episodes)
            # init dealer and player score by drawing a black card
            dealer_score, _ = easy21.draw()
            player_score, _ = easy21.draw()

            # create state (dealer score and player score)
            state = np.array([dealer_score, player_score])

            # init elegibility traces to zero
            E = np.zeros((36,))

            # step size
            alpha = 0.01

            # epsilon
            eps = 0.05

            # sample an action according to eps-greedy
            eps_action = random.random()
            if eps_action <= eps:
                action = random.randrange(0, 2, 1)
            else:
                if q(state, 0, w) > q(state, 1, w):
                    action = 0
                else:
                    action = 1

            terminal = False
            while terminal == False:
                # perform the action
                state_prime, reward, terminal = easy21.step(np.copy(state), action)

                # sample action prime according to eps-greedy
                eps_action = random.random()
                if eps_action <= eps:
                    action_prime = random.randrange(0, 2, 1)
                else:
                    if q(state, 0, w) > q(state, 1, w):
                        action_prime = 0
                    else:
                        action_prime = 1

                # compute temporal difference error (if episode is terminal then
                # Q(s', a') is zero)
                if terminal == True:
                    td_error = reward  - q(state, action, w)
                else:
                    td_error = reward + q(state_prime, action_prime, w) - q(state, action, w)

                # decay elegibility traces
                E = lambda_values[lambda_i] * E + theta(state, action)

                # update weights
                dw = alpha * td_error * E
                w += dw

                # set new state  and action
                state = state_prime
                action = action_prime

            # error mean squared error to correct value function
            Q = compute_q(w)
            Qmax = np.max(Q, axis=2)
            e = np.sum((Qmax - Qmax_correct) ** 2)
            learning_curve[lambda_i, nr_episode+1] = e

        # compute optimal value function
        Qmax = np.max(Q, axis=2)
        pimax = np.argmax(Q, axis=2)

        # error mean squared error to correct value function
        e = (Qmax - Qmax_correct) ** 2
        Q_error.append(np.sum(e))

        # plot final value function
        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
        X = np.arange(1, nr_states_player+1, 1)
        Y = np.arange(1, nr_states_dealer+1, 1)
        X, Y = np.meshgrid(X, Y)
        ax.plot_surface(X, Y, Qmax, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        plt.xlabel('State of player')
        plt.ylabel('State of dealer')
        plt.title('Q* value function')
        # save figure
        plt.savefig("function_approx_value_function" + str(lambda_values[lambda_i]) + ".png", dpi=150)

    # plot Q error over lambda values
    plt.figure()
    plt.plot(lambda_values, Q_error)
    plt.xlabel('Lambda')
    plt.ylabel('Mean squared error of value function')
    plt.title('Error for different lambda values')

    # save figure
    plt.savefig("function_approx_error.png", dpi=150)

    plt.figure()
    legend_entries = []
    for i in range(lambda_values.shape[0]):
        plt.plot(np.arange(nr_episodes + 1) + 1, learning_curve[i, :])
        legend_entries.append('lambda = %.1f' % (lambda_values[i]))
    plt.xlabel('Number of episode')
    plt.ylabel('Mean squared error of value function')
    # plt.legend(('lambda = 0 (TD(0))', 'lambda = 1 (Monte Carlo)'))
    plt.legend(legend_entries)
    plt.title('Learning curve for different lambda values')
    plt.show()

    print("Final error after ", nr_episodes, " episodes:")
    print(learning_curve[:, -1])
    print("for lambda = ", lambda_values)

    # save figure
    plt.savefig("function_approx_learning_curve.png", dpi=150)

if __name__ == '__main__':
    main()
