import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from Easy21 import Easy21


def main():
    random.seed(123456789)
    # create game object
    easy21 = Easy21()

    # load correct value function
    Qmax_correct = np.genfromtxt('optimal_action_value_function.csv', delimiter=',')
    # init Q error array
    Q_error = []

    # for model free control
    nr_states_dealer = 10
    nr_states_player = 21
    nr_actions = 2
    N0 = 200
    Q = np.random.rand(nr_states_dealer, nr_states_player, nr_actions)
    Q_init = np.copy(Q)


    nr_episodes = 5000
    lambda_values = np.arange(0, 1.1, 0.2)
    learning_curve = np.zeros((lambda_values.shape[0], nr_episodes+1))

    Qmax = np.max(Q, axis=2)
    e = np.sum((Qmax - Qmax_correct) ** 2)
    learning_curve[:, 0] = e

    for lambda_i in range(lambda_values.shape[0]):
        # reset Q
        N_s = np.zeros((nr_states_dealer, nr_states_player))
        N_sa = np.zeros((nr_states_dealer, nr_states_player, nr_actions))
        Q = np.copy(Q_init)
        for nr_episode in range(nr_episodes):
            # print(nr_episode, " / ", nr_episodes)
            # init dealer and player score by drawing a black card
            dealer_score, _ = easy21.draw()
            player_score, _ = easy21.draw()

            # create state (dealer score and player score)
            state = np.array([dealer_score, player_score])

            # init elegibility traces to zero
            E = np.zeros((nr_states_dealer, nr_states_player, nr_actions))

            # determine epsilon
            eps = N0 / (N0 + N_s[state[0]-1, state[1]-1])
            # sample an action according to eps-greedy
            eps_action = random.random()
            if eps_action <= eps:
                action = random.randrange(0, 2, 1)
            else:
                action = np.argmax(Q[state[0]-1, state[1]-1, :])

            terminal = False
            while terminal == False:
                # update counting arrays
                N_s[state[0]-1, state[1]-1] += 1
                N_sa[state[0]-1, state[1]-1, action] += 1

                # perform the action
                state_prime, reward, terminal = easy21.step(np.copy(state), action)

                # determine epsilon for next action
                eps = N0 / (N0 + N_s[state_prime[0]-1, state_prime[1]-1])
                # sample action prime according to eps-greedy
                eps_action = random.random()
                if eps_action <= eps:
                    action_prime = random.randrange(0, 2, 1)
                else:
                    action_prime = np.argmax(Q[state_prime[0]-1, state_prime[1]-1, :])

                # compute temporal difference error (if episode is terminal then
                # Q(s', a') is zero)
                if terminal == True:
                    td_error = reward  - Q[state[0]-1, state[1]-1, action]
                else:
                    td_error = reward + Q[state_prime[0]-1, state_prime[1]-1, action_prime] - Q[state[0]-1, state[1]-1, action]

                # increase elegibility
                E[state[0]-1, state[1]-1, action] += 1

                # compute new value function
                idx = np.where(E > 0)
                update = 1/N_sa[idx] * td_error * E[idx]
                Q[idx] += 1/N_sa[idx] * td_error * E[idx]

                # decay elegibility traces
                E *= lambda_values[lambda_i]

                # set new state  and action
                state = state_prime
                action = action_prime

            # error mean squared error to correct value function
            Qmax = np.max(Q, axis=2)
            e = np.sum((Qmax - Qmax_correct) ** 2)
            learning_curve[lambda_i, nr_episode+1] = e

        # compute optimal value function
        Qmax = np.max(Q, axis=2)
        pimax = np.argmax(Q, axis=2)

        # error mean squared error to correct value function
        e = (Qmax - Qmax_correct) ** 2
        Q_error.append(np.sum(e))

        # plot final value function
        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
        X = np.arange(1, nr_states_player+1, 1)
        Y = np.arange(1, nr_states_dealer+1, 1)
        X, Y = np.meshgrid(X, Y)
        ax.plot_surface(X, Y, Qmax, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        plt.xlabel('State of player')
        plt.ylabel('State of dealer')
        plt.title('Q* value function')
        # save figure
        plt.savefig("sarsa_lambda_value_function" + str(lambda_values[lambda_i]) + ".png", dpi=150)

    # plot Q error over lambda values
    plt.figure()
    plt.plot(lambda_values, Q_error)
    plt.xlabel('Lambda')
    plt.ylabel('Mean squared error of value function')
    plt.title('Error for different lambda values')

    # save figure
    plt.savefig("sarsa_lambda_error.png", dpi=150)

    plt.figure()
    legend_entries = []
    for i in range(lambda_values.shape[0]):
        plt.plot(np.arange(nr_episodes + 1) + 1, learning_curve[i, :])
        legend_entries.append('lambda = %.1f' % (lambda_values[i]))
    plt.xlabel('Number of episode')
    plt.ylabel('Mean squared error of value function')
    # plt.legend(('lambda = 0 (TD(0))', 'lambda = 1 (Monte Carlo)'))
    plt.legend(legend_entries)
    plt.title('Learning curve for different lambda values')
    plt.show()

    print("Final error after ", nr_episodes, " episodes:")
    print(learning_curve[:, -1])
    print("for lambda = ", lambda_values)

    # save figure
    plt.savefig("sarsa_lambda_learning_curve.png", dpi=150)

if __name__ == '__main__':
    main()
