import random
import numpy as np

class Easy21:
    def __init__(self):
        # probabilities for colors
        self.P_RED = 1/3
        self.P_BLACK = 1 - self.P_RED

        # actions
        self.A_STICK = 0
        self.A_HIT = 1

    def step(self, state, action):
        terminal = False
        reward = 0
        if action == self.A_STICK:
            terminal = True
            # dealers turn
            dealer_state = state[0]
            while dealer_state < 17 and dealer_state > 0:
                value, color = self.draw()
                dealer_state += np.max((0, color * value))

            if dealer_state > 21 or dealer_state < 1:
                # dealer lost
                reward = 1
            elif dealer_state < state[1]:
                # player won
                reward = 1
            elif dealer_state > state[1]:
                # dealer won
                reward = -1
            elif dealer_state == state[1]:
                # draw
                reward = 0

        elif action == self.A_HIT:
            value, color = self.draw()
            new_player_state = state[1] + color * value

            # check if we lost the game
            if new_player_state > 21 or new_player_state < 1:
                terminal = True
                reward = -1
            else:
                state[1] = new_player_state
        else:
            print("[ERROR] invalid action use %d or %d as actions" %
                    (self.A_STICK, self.A_HIT))

        # create new state
        new_state = state
        
        return new_state, reward, terminal


    def draw(self):
        color = random.random()
        value = random.randrange(1, 11, 1)
        if color <= self.P_RED:
            return value, -1
        else:
            return value, 1

