# Easy21 implementation according to RL course

Implementation of the assignment of the RL course of David Silver. Assignment
can be found [here](https://www.davidsilver.uk/wp-content/uploads/2020/03/Easy21-Johannes.pdf).

The assignment is to use several basic RL algorithms and apply them to the Easy
21 game which is a game similar to Black Jack. 

## Monte Carlo control

First task is to use Monte Carlo control. 
This is used to find the optimal value function that Monte Carlo control
converges to. Here 400000 episodes are used to obtain the following result:

<img src="plots/monte_carlo_value_function.png" width="500"/>

As expected, the value function increases when the player has a score close to
21. The small visible bump is due to the fact that it is advantageous to have a
score in this region because you are unlikely to go bust but have a high chance
to get a good score. The value function also decreases slightly when the dealer
is showing a high card because than he is more likely to win. 

The optimal policy can be seen here:

<img src="plots/monte_carlo_policy.png" width="500"/>

Action 1 means hitting and 0 means sticking. This is interesting because it is
only optimal to hit when the value is above a certain threshold meaning that
beneath this value it is better to hope that the dealer goes bust and wait it
out. This is because low player values have the risk that the player goes bust
by going negative.

## Sarsa Lambda Control

Changing Monte Carlo control to Sarsa Lambda control uses updates every step and
therefore often converges faster. The deviation error from the optimal value
function can for different lambda values after 5000 iterations can be seen in
the following:

<img src="plots/sarsa_lambda_error.png" width="500"/>

The learning curves can be seen in the next picture where is becomes clear which
lambda values are the best ones.

<img src="plots/sarsa_lambda_learning_curve.png" width="500"/>

As can be seen, for lambda = 0.4 the value function converges the quickest. For
the present case, this value seems to strike the best balance between updating
the values each step and keeping a memory of the past states for updating. 

## Function approximation

Since often in RL problems the state space becomes intractable large, it is
necessary to use function approximation. In this case coarse coding for
approximation. The deviation error from the optimal value
function can for different lambda values after 5000 iterations can be seen in
the following:


<img src="plots/function_approx_error.png" width="500"/>

The learning curves can be seen in the next picture where is becomes clear which
lambda values are the best ones.

<img src="plots/function_approx_learning_curve.png" width="500"/>

As can be seen, all lambda values converge at similar speed and in general
converge much faster than Sarsa Lambda. This is because there is much less
values to learn and the function approximator then generalizes. This leads to a
less sharp fit though. This can be seen from the highest value in the function
being lower than for Sarsa Lambda. The remaining error will thus be higher. 

## Value function comparison

Lets compare the value function between the 3 methods after 5000 iterations. 
As you can see in the following plots, the Sarsa Lambda method already has a
good value function while the Monte Carlo result is very noisy. This is to be
expected since the Sarsa Lambda algorithm updates the value function at every
time step whereas Monte Carlo only once at the end of the episode.
The function approximation has a decent result but worse that Sarsa Lambda. This
is caused by the approximation error but it needs much less data to represent
the value function. It is easy to see the coarse coding patches in the plot. 

__Monte Carlo__

<img src="plots/monte_carlo_value_function5000.png" width="500"/>

__Sarsa Lambda (lambda = 0.4)__

<img src="plots/sarsa_lambda_value_function04.png" width="500"/>

__Function approximation (lambda = 0.6)__

<img src="plots/function_approx_value_function06.png" width="500"/>


To further show the differences, the final errors of the methods for different
lambda values are shown:

| Lambda | Error Sarsa Lambda | Error function approximation |
| --- | --- | --- |
| 0 | 10.71| 13.51|
| 0.2 | 12.06| 12.96| 
| 0.4 | 9.69| 14.78|
| 0.6 | 15.68| 13.66|
| 0.8 | 11.5| 18.67| 
| 1 | 10.29| 26.53|


Monte Carlo error: 16.83 

Here we can also see that even though the function approximator converges much
faster it remains at a higher error level. Both methods outperform the Monte
Carlo method except for high lambda values of the function approximator. This is
likely due to the approximation error.
