import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from Easy21 import Easy21


def main():
    random.seed(123456789)
    # create game object
    easy21 = Easy21()

    # for model free control
    nr_states_dealer = 10
    nr_states_player = 21
    nr_actions = 2
    N0 = 500
    N_s = np.zeros((nr_states_dealer, nr_states_player))
    N_sa = np.zeros((nr_states_dealer, nr_states_player, nr_actions))
    Q = np.zeros_like(N_sa)

    # set number of episodes
    nr_episodes = 4000000

    # run the episodes
    for nr_episode in range(nr_episodes):
        # init dealer and player score by drawing a black card
        dealer_score, _ = easy21.draw()
        player_score, _ = easy21.draw()

        # create state (dealer score and player score)
        state = np.array([dealer_score, player_score])

        terminal = False
        trace = []
        while terminal == False:
            # determine epsilon
            eps = N0 / (N0 + N_s[state[0]-1, state[1]-1])
            # sample an action according to eps-greedy
            eps_action = random.random()
            if eps_action <= eps:
                action = random.randrange(0, 2, 1)
            else:
                action = np.argmax(Q[state[0]-1, state[1]-1, :])

            # append state and action to trace (in case it is not in there
            # already), this is only first visit monte carlo control
            trace_elem = [state[0]-1, state[1]-1, action]
            if trace_elem not in trace:
                trace.append(trace_elem)

            # update counting arrays
            N_s[state[0]-1, state[1]-1] += 1
            N_sa[state[0]-1, state[1]-1, action] += 1

            # perform the action
            state, reward, terminal = easy21.step(state, action)

        # episode finished so update the value function
        for s in trace:
            Q[s[0], s[1], s[2]] += (reward - Q[s[0], s[1], s[2]]) / N_sa[s[0], s[1], s[2]]

    # compute optimal value function
    Qmax = np.max(Q, axis=2)
    pimax = np.argmax(Q, axis=2)
    plt.show()

    np.savetxt('optimal_action_value_function.csv', Qmax, delimiter=',')
    # np.savetxt('optimal_action_value_function.csv', Qmax, delimiter=',')

    # plot final value function
    _, ax = plt.subplots(subplot_kw={"projection": "3d"})
    X = np.arange(1, nr_states_player+1, 1)
    Y = np.arange(1, nr_states_dealer+1, 1)
    X, Y = np.meshgrid(X, Y)
    ax.plot_surface(X, Y, Qmax, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.title('Q* value function')
    plt.xlabel('State of player')
    plt.ylabel('State of dealer')
    
    # save figure
    print(e)

    # plot policy
    _, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, pimax, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.title('pi* policy')
    plt.xlabel('State of player')
    plt.ylabel('State of dealer')

    # save figure
    plt.savefig("monte_carlo_policy.png", dpi=150)

    # show plots
    plt.show()

if __name__ == '__main__':
    main()
